package com.wiceo.simpleweather.adapters;

/**
 * Created by master on 06.11.2015.
 */

import android.content.Context;
import android.location.Address;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wiceo.simpleweather.R;

import java.util.List;

public class SearchArrayAdapter extends ArrayAdapter<Address> {
    private Context context;
    private List<Address> results;



    public SearchArrayAdapter(Context context, List<Address> results) {
        super(context, R.layout.search_listview_item, results);
        this.context = context;
        this.results = results;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = convertView;

        if(convertView == null) {
            rowView = inflater.inflate(R.layout.search_listview_item, parent, false);
        }
        final VHolder holder = new VHolder();

        holder.result = results.get(position);
        holder.text = (TextView) rowView.findViewById(R.id.search_item_text);
        if (holder.result.getLocality() != null && !holder.result.getLocality().equals("")) {
            holder.text.setText(holder.result.getCountryName() + ", " + holder.result.getLocality());
        } else {
            holder.text.setText(holder.result.getCountryName());
        }


        rowView.setTag(holder.result);
        return rowView;
    }

    public static class VHolder {
        Address result;
        TextView text;
    }
}