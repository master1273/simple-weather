package com.wiceo.simpleweather;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.wiceo.simpleweather.adapters.MyPagerAdapter;


public class MainActivity extends AppCompatActivity {

    public static ViewPager pager;
    private AdView mAdView;

    private boolean doubleBackToExitPressedOnce;
    private Handler backHandler = new Handler();
    private final Runnable backRunnable = new Runnable() {
        @Override
        public void run() {
            doubleBackToExitPressedOnce = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.app_name));

        // Initialize the ViewPager and set an adapter
        pager = (ViewPager) findViewById(R.id.main_pager);
        pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));

        // Bind the tabs to the ViewPager
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.main_tabs);
        tabs.setShouldExpand(true);
        tabs.setViewPager(pager);
        tabs.setTextColor(getResources().getColor(R.color.white));


        mAdView = (AdView) this.findViewById(R.id.ad_view);
        if (mAdView!= null) {
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    //.addTestDevice(Constants.TEST_DEVICE_ID)
                    .build();

            // Start loading the ad in the background.
            mAdView.loadAd(adRequest);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent i;
        switch (item.getItemId()){
            case R.id.action_change_city:
                i = new Intent(MainActivity.this, ChooseCityActivity.class);
                i.putExtra("flag_from_main_activity", true);
                startActivity(i);
                MainActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                return true;
            case R.id.action_feedback:
                i = new Intent(MainActivity.this, FeedbackActivity.class);
                startActivity(i);
                MainActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                return true;
            case R.id.action_rate_app:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=com.wiceo.simpleweather"));
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, ApplicationContextProvider.getContext().getResources().getString(R.string.back_message), Toast.LENGTH_SHORT).show();

        backHandler.postDelayed(backRunnable, Constants.POST_DELAY_FOR_EXIT);

    }
}
