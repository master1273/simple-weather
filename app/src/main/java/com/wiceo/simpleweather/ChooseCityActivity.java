package com.wiceo.simpleweather;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.wiceo.simpleweather.adapters.SearchArrayAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Pavel on 05.11.2015.
 */
public class ChooseCityActivity extends AppCompatActivity implements OnMapReadyCallback {


    private GoogleMap map;
    public MarkerOptions markerOptions;

    public Address finalAddress = null;

    private boolean flag = false;

    private ImageView voiceInputImage;
    private ImageView clearText;
    private EditText searchEdit = null;
    private SearchArrayAdapter resultsAdapter;
    private ListView searchResults = null;


    private boolean doubleBackToExitPressedOnce;
    private Handler backHandler = new Handler();
    private final Runnable backRunnable = new Runnable() {
        @Override
        public void run() {
            doubleBackToExitPressedOnce = false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String languageToLoad = "en_US";
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_choose_city);

        if (!getIntent().getExtras().getBoolean("flag_from_main_activity", false)) {
            flag = getIntent().getExtras().getBoolean("flag_from_main_activity", false);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_city);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.choose_city));


        // Getting Google Play availability status
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

        // Showing status
        if(status!= ConnectionResult.SUCCESS)
        { // Google Play Services are not available
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        }
        else {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);

            if (savedInstanceState == null) {
                // First incarnation of this activity.
                mapFragment.setRetainInstance(true);
            }
            else
            {
                // Reincarnated activity. The obtained map is the same map instance in the previous
                // activity life cycle. There is no need to reinitialize it.
                map = mapFragment.getMap();
            }
        }

        searchResults = (ListView) findViewById(R.id.search_results);
        searchEdit = (EditText) findViewById(R.id.search_edittext);
        searchEdit.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (! "".equals(s.toString())){
                    clearText.setVisibility(View.VISIBLE);
                    Geocoder gc = new Geocoder(getBaseContext());
                    try{
                        List<Address> list = gc.getFromLocationName(s.toString(), 10);
                        fillListView(list);
                    } catch (IOException e){
                        e.printStackTrace();
                    }
                } else {
                    clearText.setVisibility(View.INVISIBLE);
                    fillListView(null);
                    map.clear();
                    finalAddress = null;
                }
            }
        });
        clearText = (ImageView) findViewById(R.id.clear_text);
        clearText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.playSoundEffect(SoundEffectConstants.CLICK);
                searchEdit.setText("");
                clearText.setVisibility(View.INVISIBLE);
                map.clear();
                finalAddress = null;
            }
        });

        voiceInputImage = (ImageView) findViewById(R.id.voice_input_image);

        PackageManager pm = getPackageManager();
        List activities = pm.queryIntentActivities(new Intent(
                RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
        if (activities.size() != 0) {
            voiceInputImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    voiceInputImage.setImageResource(R.drawable.mic_dark);
                    startVoiceRecognitionActivity();
                }
            });
        } else {
            voiceInputImage.setVisibility(View.GONE);
        }
    }

    private void setUpMapIfNeeded(){

        if (map == null)
            map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();

        if (map != null)
        {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

            // Enabling MyLocation Layer of Google Map
            map.setTrafficEnabled(false);
            //map.getUiSettings().setCompassEnabled(true);
            map.getUiSettings().setZoomControlsEnabled(true);
            //map.getUiSettings().setIndoorLevelPickerEnabled(true);

            map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng arg0) {
                    map.clear();
                    // Animating to the touched position
                    map.animateCamera(CameraUpdateFactory.newLatLng(arg0));
                    // Creating a marker
                    markerOptions = new MarkerOptions();

                    // Setting the position for the marker
                    markerOptions.position(arg0);

                    Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.marker);
                    Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, 100, 100, false);
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(resizedBitmap));
                    // Placing a marker on the touched position
                    map.addMarker(markerOptions);

                    // Adding Marker on the touched location with address
                    new ReverseGeocodingTask(getBaseContext()).execute(arg0);
                }
            });
        }
    }

    private class ReverseGeocodingTask extends AsyncTask<LatLng, Void, Address> {
        Context mContext;

        public ReverseGeocodingTask(Context context){
            super();
            mContext = context;
        }

        // Finding address using reverse geocoding
        @Override
        protected Address doInBackground(LatLng... params) {
            Address adr = null;
            Geocoder geocoder = new Geocoder(mContext);
            double latitude = params[0].latitude;
            double longitude = params[0].longitude;

            List<Address> addresses = null;
            String addressText="";

            try {
                addresses = geocoder.getFromLocation(latitude, longitude,1);
            } catch (IOException e) {
                e.printStackTrace();
                finalAddress = new Address(Locale.ENGLISH);
                finalAddress.setLatitude(latitude);
                finalAddress.setLongitude(longitude);

            }

            if(addresses != null && addresses.size() > 0 ){
                Address address = addresses.get(0);

                adr = addresses.get(0);
                addressText = String.format("%s, %s, %s",
                        address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
                        address.getLocality(),
                        address.getCountryName());
            }

            return adr;
        }

        @Override
        protected void onPostExecute(Address address) {
            //address = addressText;
            if (address != null) {
                if (address.getLocality() != null && !address.getLocality().equals("")) {
                    searchEdit.setText(address.getCountryName() + ", " + address.getLocality());
                } else {
                    searchEdit.setText(address.getCountryName());
                }
                searchResults.setAdapter(null);
                searchResults.setVisibility(View.GONE);
                finalAddress = address;
            }
        }
    }

    public void fillListView(List<Address> list) {
        if (list != null && !list.isEmpty()) {
            searchResults.setVisibility(View.VISIBLE);
            resultsAdapter = new SearchArrayAdapter(ChooseCityActivity.this, list);
            searchResults.setAdapter(resultsAdapter);
            searchResults.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Address adr = (Address) view.getTag();
                    finalAddress = adr;

                    map.clear();
                    // Animating to the touched position
                    map.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(adr.getLatitude(), adr.getLongitude())));
                    // Creating a marker
                    markerOptions = new MarkerOptions();

                    // Setting the position for the marker
                    markerOptions.position(new LatLng(adr.getLatitude(), adr.getLongitude()));

                    Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.marker);
                    Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, 100, 100, false);
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(resizedBitmap));
                    // Placing a marker on the touched position
                    map.addMarker(markerOptions);

                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

                    if (adr.getLocality() != null && !adr.getLocality().equals("")) {
                        searchEdit.setText(adr.getCountryName() + ", " + adr.getLocality());
                    } else {
                        searchEdit.setText(adr.getCountryName());
                    }

                    searchResults.setAdapter(null);
                    searchResults.setVisibility(View.GONE);
                }
            });
        } else {
            searchResults.setAdapter(null);
            searchResults.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        setUpMapIfNeeded();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onBackPressed() {
        if (flag){
            Intent i = new Intent(ChooseCityActivity.this, MainActivity.class);
            startActivity(i);
            ChooseCityActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            finish();
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, ApplicationContextProvider.getContext().getResources().getString(R.string.back_message), Toast.LENGTH_SHORT).show();

            backHandler.postDelayed(backRunnable, Constants.POST_DELAY_FOR_EXIT);
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {

    }

    public void startVoiceRecognitionActivity() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                ApplicationContextProvider.getStringRes(R.string.voice_search_hint));
        startActivityForResult(intent, Constants.VOICE_RECOGNITION_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.VOICE_RECOGNITION_REQUEST_CODE) {
            voiceInputImage.setImageResource(R.drawable.mic_light);
            if (resultCode == RESULT_OK) {
                ArrayList matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

                if (!matches.isEmpty()){
                    searchEdit.setText((String)matches.get(0));
                } else {
                    Toast.makeText(ApplicationContextProvider.getContext(), ApplicationContextProvider.getStringRes(R.string.nothing_found_by_voice), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.city_choose_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.action_save:
                if (finalAddress != null){
                    SharedPreferences.Editor ed = ApplicationContextProvider.getPrefs().edit();
                    Utils.putDouble(ed, "lat", finalAddress.getLatitude());
                    Utils.putDouble(ed, "lon", finalAddress.getLongitude());
                    ed.commit();
                    Intent intent = new Intent(ChooseCityActivity.this, MainActivity.class);
                    ChooseCityActivity.this.startActivity(intent);
                    ChooseCityActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    ChooseCityActivity.this.finish();
                } else {
                    Toast.makeText(ChooseCityActivity.this, ApplicationContextProvider.getStringRes(R.string.city_not_specified), Toast.LENGTH_SHORT).show();
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
