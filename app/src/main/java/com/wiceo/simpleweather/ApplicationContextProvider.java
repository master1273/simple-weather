package com.wiceo.simpleweather;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.support.multidex.MultiDex;

/**
 * Created by Pavel on 05.11.2015.
 */
public class ApplicationContextProvider extends Application {
    private static Context sContext;

    private static AssetManager assetManager;
    private static Resources res;
    private static SharedPreferences sPrefs;
    private static String PREFS_NAME = "app_preferences";


    @Override
    public void onCreate() {
        super.onCreate();

        sContext = getApplicationContext();
        assetManager = getResources().getAssets();
        res = getResources();
        sPrefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static Context getContext() {
        return sContext;
    }

    public static AssetManager getAssetManager () {
        return assetManager;
    }

    public static String getStringRes(int stringId){
        return res.getString(stringId);
    }

    public static int getColorRes(int colorId){
        return res.getColor(colorId);
    }

    public static SharedPreferences getPrefs(){
        return sPrefs;
    }
}