package com.wiceo.simpleweather.parsers;

import com.wiceo.simpleweather.types.Forecast;
import com.wiceo.simpleweather.types.FutureWeather;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by master on 07.11.2015.
 */
public class ForecastParser {
    public static void parseForecast(InputStream inputStream, Forecast forecast)
    {
        SAXBuilder saxBuilder = new SAXBuilder();
        try {
            Document document = saxBuilder.build(inputStream);
            Element rootNode = document.getRootElement();

            forecast.setCityName(rootNode.getChild("location").getChildText("name"));
            forecast.setCountryName(rootNode.getChild("location").getChildText("country"));
            if (rootNode.getChild("location").getChild("location").getAttributeValue("latitude") == null || rootNode.getChild("location").getChild("location").getAttributeValue("latitude").equals("")) {
                forecast.setLat(0);
            } else {
                forecast.setLat(Double.parseDouble(rootNode.getChild("location").getChild("location").getAttributeValue("latitude")));
            }

            if (rootNode.getChild("location").getChild("location").getAttributeValue("longitude") == null || rootNode.getChild("location").getChild("location").getAttributeValue("longitude").equals("")) {
                forecast.setLon(0);
            } else {
                forecast.setLon(Double.parseDouble(rootNode.getChild("location").getChild("location").getAttributeValue("longitude")));
            }

            if (rootNode.getChild("meta").getChildText("calctime") == null || rootNode.getChild("meta").getChildText("calctime").equals("")) {
                forecast.setCalcTime(0);
            } else {
                forecast.setCalcTime(Double.parseDouble(rootNode.getChild("meta").getChildText("calctime")));
            }

            List<Element> forecast_list = rootNode.getChild("forecast").getChildren();
            for(Element e : forecast_list) {

                FutureWeather futureWeather = new FutureWeather();
                futureWeather.setTimeFrom(e.getAttributeValue("from"));
                futureWeather.setTimeTo(e.getAttributeValue("to"));

                SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
                try {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(sdf.parse(futureWeather.getTimeFrom().replace("T", " ")));
                    if (cal.compareTo(Calendar.getInstance()) < 0){
                        continue;
                    }
                } catch (ParseException pe){
                    pe.printStackTrace();
                }


                futureWeather.setIcon(e.getChild("symbol").getAttributeValue("var"));

                if (e.getChild("precipitation").getAttributeValue("value") == null || e.getChild("precipitation").getAttributeValue("value").equals("")) {
                    futureWeather.setPressureValue(0);
                } else {
                    futureWeather.setPrecipitationValue(Double.parseDouble(e.getChild("precipitation").getAttributeValue("value")));
                }
                futureWeather.setPrecipitationType(e.getChild("precipitation").getAttributeValue("type"));
                futureWeather.setPrecipitationUnit(e.getChild("precipitation").getAttributeValue("unit"));

                futureWeather.setWindDirection(e.getChild("windDirection").getAttributeValue("name"));
                if (e.getChild("windSpeed").getAttributeValue("mps") == null || e.getChild("windSpeed").getAttributeValue("mps").equals("")) {
                    futureWeather.setWindSpeed(0);
                } else {
                    futureWeather.setWindSpeed(Double.parseDouble(e.getChild("windSpeed").getAttributeValue("mps")));
                }

                futureWeather.setWindName(e.getChild("windSpeed").getAttributeValue("name"));

                if (e.getChild("temperature").getAttributeValue("min") == null || e.getChild("temperature").getAttributeValue("min").equals("")) {
                    futureWeather.setMinTemp(0);
                } else {
                    futureWeather.setMinTemp(Double.parseDouble(e.getChild("temperature").getAttributeValue("min")));
                }
                if (e.getChild("temperature").getAttributeValue("value") == null || e.getChild("temperature").getAttributeValue("value").equals("")) {
                    futureWeather.setAvrTemp(0);
                } else {
                    futureWeather.setAvrTemp(Double.parseDouble(e.getChild("temperature").getAttributeValue("value")));
                }
                if (e.getChild("temperature").getAttributeValue("max") == null || e.getChild("temperature").getAttributeValue("max").equals("")) {
                    futureWeather.setMaxTemp(0);
                } else {
                    futureWeather.setMaxTemp(Double.parseDouble(e.getChild("temperature").getAttributeValue("max")));
                }


                futureWeather.setPressureUnit(e.getChild("pressure").getAttributeValue("unit"));
                futureWeather.setPressureValue(Double.parseDouble(e.getChild("pressure").getAttributeValue("value")));

                futureWeather.setHumidityUnit(e.getChild("humidity").getAttributeValue("unit"));
                if (e.getChild("humidity").getAttributeValue("value") == null || e.getChild("humidity").getAttributeValue("value").equals("")) {
                    futureWeather.setHumidityValue(0);
                } else {
                    futureWeather.setHumidityValue(Double.parseDouble(e.getChild("humidity").getAttributeValue("value")));
                }

                futureWeather.setCloudsUnit(e.getChild("clouds").getAttributeValue("unit"));
                if (e.getChild("clouds").getAttributeValue("all") == null || e.getChild("clouds").getAttributeValue("all").equals("")) {
                    futureWeather.setCloudsValue(0);
                } else {
                    futureWeather.setCloudsValue(Double.parseDouble(e.getChild("clouds").getAttributeValue("all")));
                }

                forecast.addFutureWeather(futureWeather);
            }
        }

        catch (JDOMException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}