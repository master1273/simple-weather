package com.wiceo.simpleweather.types;

/**
 * Created by maste on 07.11.2015.
 */
public class FutureWeather {
    private String timeFrom;
    private String timeTo;
    private String icon;
    private String precipitationUnit;
    private double precipitationValue;
    private String precipitationType;
    private String windDirection;
    private double windSpeed;
    private String windName;
    private double minTemp;
    private double avrTemp;
    private double maxTemp;
    private double pressureValue;
    private String pressureUnit;
    private double humidityValue;
    private String humidityUnit;
    private double cloudsValue;
    private String cloudsUnit;

    public double getAvrTemp() {
        return avrTemp;
    }

    public double getCloudsValue() {
        return cloudsValue;
    }

    public double getHumidityValue() {
        return humidityValue;
    }

    public double getMaxTemp() {
        return maxTemp;
    }

    public double getMinTemp() {
        return minTemp;
    }

    public double getPrecipitationValue() {
        return precipitationValue;
    }

    public double getPressureValue() {
        return pressureValue;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public String getCloudsUnit() {
        return cloudsUnit;
    }

    public String getHumidityUnit() {
        return humidityUnit;
    }

    public String getIcon() {
        return icon;
    }

    public String getPrecipitationType() {
        return precipitationType;
    }

    public String getPrecipitationUnit() {
        return precipitationUnit;
    }

    public String getPressureUnit() {
        return pressureUnit;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public String getWindDirection() {
        return windDirection;
    }

    public String getWindName() {
        return windName;
    }

    public void setAvrTemp(double avrTemp) {
        this.avrTemp = avrTemp;
    }

    public void setCloudsUnit(String cloudsUnit) {
        this.cloudsUnit = cloudsUnit;
    }

    public void setCloudsValue(double cloudsValue) {
        this.cloudsValue = cloudsValue;
    }

    public void setHumidityUnit(String humidityUnit) {
        this.humidityUnit = humidityUnit;
    }

    public void setHumidityValue(double humidityValue) {
        this.humidityValue = humidityValue;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setMaxTemp(double maxTemp) {
        this.maxTemp = maxTemp;
    }

    public void setMinTemp(double minTemp) {
        this.minTemp = minTemp;
    }

    public void setPrecipitationType(String precipitationType) {
        this.precipitationType = precipitationType;
    }

    public void setPrecipitationUnit(String precipitationUnit) {
        this.precipitationUnit = precipitationUnit;
    }

    public void setPrecipitationValue(double precipitationValue) {
        this.precipitationValue = precipitationValue;
    }

    public void setPressureUnit(String pressureUnit) {
        this.pressureUnit = pressureUnit;
    }

    public void setPressureValue(double pressureValue) {
        this.pressureValue = pressureValue;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    public void setWindDirection(String windDirection) {
        this.windDirection = windDirection;
    }

    public void setWindName(String windName) {
        this.windName = windName;
    }

    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }
}
