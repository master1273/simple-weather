package com.wiceo.simpleweather.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.wiceo.simpleweather.ApplicationContextProvider;
import com.wiceo.simpleweather.R;
import com.wiceo.simpleweather.fragments.ForecastFragment;
import com.wiceo.simpleweather.fragments.InfoFragment;


/**
 * Created by Pavel on 05.11.2015.
 */
public class MyPagerAdapter extends FragmentPagerAdapter {

    private final String[] TITLES = {ApplicationContextProvider.getStringRes(R.string.info_tab),
            ApplicationContextProvider.getStringRes(R.string.forecast_tab)};

    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return InfoFragment.newInstance();
            case 1:
                return ForecastFragment.newInstance();
            default:
                return null;
        }
    }
}