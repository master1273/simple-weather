package com.wiceo.simpleweather.types;

/**
 * Created by maste on 06.11.2015.
 */
public class CurrentWeather {
    private int cityID;
    private String cityName;
    private double lon;
    private double lat;
    private String country;
    private String sunSet;
    private String sunRise;
    private double maxTemp;
    private double avrTemp;
    private double minTemp;
    private double humidity;
    private String humidityValue;
    private double pressure;
    private String pressureValue;
    private double windSpeed;
    private String windName;
    private double directionValue;
    private String directionName;
    private double cloudsValue;
    private String cloudsName;
    private String icon;
    private String lastUpdated;

    public double getAvrTemp() {
        return avrTemp;
    }

    public double getCloudsValue() {
        return cloudsValue;
    }

    public double getDirectionValue() {
        return directionValue;
    }

    public double getHumidity() {
        return humidity;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public double getMaxTemp() {
        return maxTemp;
    }

    public double getMinTemp() {
        return minTemp;
    }

    public double getPressure() {
        return pressure;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public int getCityID() {
        return cityID;
    }

    public String getCityName() {
        return cityName;
    }

    public String getCloudsName() {
        return cloudsName;
    }

    public String getCountry() {
        return country;
    }

    public String getDirectionName() {
        return directionName;
    }

    public String getHumidityValue() {
        return humidityValue;
    }

    public String getIcon() {
        return icon;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public String getPressureValue() {
        return pressureValue;
    }

    public String getSunRise() {
        return sunRise;
    }

    public String getSunSet() {
        return sunSet;
    }

    public String getWindName() {
        return windName;
    }

    public void setAvrTemp(double avrTemp) {
        this.avrTemp = avrTemp;
    }

    public void setCityID(int cityID) {
        this.cityID = cityID;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public void setCloudsName(String cloudsName) {
        this.cloudsName = cloudsName;
    }

    public void setCloudsValue(double cloudsValue) {
        this.cloudsValue = cloudsValue;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setDirectionName(String directionName) {
        this.directionName = directionName;
    }

    public void setDirectionValue(double directionValue) {
        this.directionValue = directionValue;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public void setHumidityValue(String humidityValue) {
        this.humidityValue = humidityValue;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public void setMaxTemp(double maxTemp) {
        this.maxTemp = maxTemp;
    }

    public void setMinTemp(double minTemp) {
        this.minTemp = minTemp;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public void setPressureValue(String pressureValue) {
        this.pressureValue = pressureValue;
    }

    public void setSunRise(String sunRise) {
        this.sunRise = sunRise;
    }

    public void setSunSet(String sunSet) {
        this.sunSet = sunSet;
    }

    public void setWindName(String windName) {
        this.windName = windName;
    }

    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }
}
