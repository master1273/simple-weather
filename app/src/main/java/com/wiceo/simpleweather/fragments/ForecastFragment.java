package com.wiceo.simpleweather.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.wiceo.simpleweather.ApplicationContextProvider;
import com.wiceo.simpleweather.R;
import com.wiceo.simpleweather.Utils;
import com.wiceo.simpleweather.adapters.ForecastArrayAdapter;
import com.wiceo.simpleweather.parsers.ForecastParser;
import com.wiceo.simpleweather.types.Forecast;

import java.net.URL;
import java.net.URLConnection;

/**
 * Created by master on 07.11.2015.
 */
public class ForecastFragment extends Fragment {

    private ListView listView;
    private TextView noForecast;

    public static ForecastFragment newInstance() {
        ForecastFragment f = new ForecastFragment();
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.forecast_fragment, container, false);

        GetForecast getter = new GetForecast(getLink(Utils.getDouble(ApplicationContextProvider.getPrefs(), "lat", 0),
                Utils.getDouble(ApplicationContextProvider.getPrefs(), "lon", 0)));
        getter.start();

        noForecast = (TextView) view.findViewById(R.id.no_forecast);
        listView = (ListView) view.findViewById(R.id.forecast_listview);

        return view;
    }

    private String getLink(double lat, double lon){
        return "http://api.openweathermap.org/data/2.5/forecast?lat="
                + Double.toString(lat)
                + "&lon="
                + Double.toString(lon)
                + "&units=metric"
                + "&mode=xml&appid="
                + ApplicationContextProvider.getStringRes(R.string.open_weather_api_key);
    }

    private void fillData(Forecast forecast){
        if (!forecast.getFutureWeathers().isEmpty()){
            listView.setVisibility(View.VISIBLE);
            noForecast.setVisibility(View.GONE);

            ForecastArrayAdapter adapter = new ForecastArrayAdapter(getActivity().getApplicationContext(), forecast.getFutureWeathers());
            listView.setAdapter(adapter);

        } else {
            listView.setVisibility(View.GONE);
            noForecast.setVisibility(View.VISIBLE);
        }
    }

    private class GetForecast extends Thread {
        private String link;

        public GetForecast (String link){
            this.link = link;
        }

        @Override
        public void run() {
            final Forecast forecast = new Forecast();
            Boolean flag = true;
            try {
                URL url = new URL(link);
                URLConnection conn = url.openConnection();
                ForecastParser.parseForecast(conn.getInputStream(), forecast);

            } catch (Exception e){
                e.printStackTrace();
                flag = false;
            } finally {
                if (flag){
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            fillData(forecast);
                        }
                    });
                }
            }
            Thread.currentThread().interrupt();
        }
    }
}