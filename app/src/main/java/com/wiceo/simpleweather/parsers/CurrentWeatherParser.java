package com.wiceo.simpleweather.parsers;

import com.wiceo.simpleweather.types.CurrentWeather;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by maste on 06.11.2015.
 */
public class CurrentWeatherParser {
    public static void parseCurrentWeather(InputStream inputStream, CurrentWeather currentWeather)
    {
        SAXBuilder saxBuilder = new SAXBuilder();
        try {
            Document document = saxBuilder.build(inputStream);
            Element rootNode = document.getRootElement();

            Element city = rootNode.getChild("city");

            if (city.getAttributeValue("id") == null || city.getAttributeValue("id").equals("")){
                currentWeather.setCityID(0);
            } else {
                currentWeather.setCityID((Integer.parseInt(city.getAttributeValue("id"))));
            }
            currentWeather.setCityName(city.getAttributeValue("name"));

            if (city.getChild("coord").getAttributeValue("lon") == null || city.getChild("coord").getAttributeValue("lon").equals("")) {
                currentWeather.setLat(0);
            } else {
                currentWeather.setLat(Double.parseDouble(city.getChild("coord").getAttributeValue("lat")));
            }

            if (city.getChild("coord").getAttributeValue("lon") == null || city.getChild("coord").getAttributeValue("lon").equals("")) {
                currentWeather.setLon(0);
            } else {
                currentWeather.setLon(Double.parseDouble(city.getChild("coord").getAttributeValue("lon")));
            }

            currentWeather.setCountry(city.getChildText("country"));

            currentWeather.setSunRise(city.getChild("sun").getAttributeValue("rise"));
            currentWeather.setSunSet(city.getChild("sun").getAttributeValue("set"));

            if (rootNode.getChild("temperature").getAttributeValue("value") == null || rootNode.getChild("temperature").getAttributeValue("value").equals("")){
                currentWeather.setAvrTemp(0);
            } else {
                currentWeather.setAvrTemp(Double.parseDouble(rootNode.getChild("temperature").getAttributeValue("value")));
            }
            if (rootNode.getChild("temperature").getAttributeValue("min") == null || rootNode.getChild("temperature").getAttributeValue("min").equals("")){
                currentWeather.setMinTemp(0);
            } else {
                currentWeather.setMinTemp(Double.parseDouble(rootNode.getChild("temperature").getAttributeValue("min")));
            }
            if (rootNode.getChild("temperature").getAttributeValue("max") == null || rootNode.getChild("temperature").getAttributeValue("max").equals("")){
                currentWeather.setMaxTemp(0);
            } else {
                currentWeather.setMaxTemp(Double.parseDouble(rootNode.getChild("temperature").getAttributeValue("max")));
            }

            if (rootNode.getChild("humidity").getAttributeValue("value") == null || rootNode.getChild("humidity").getAttributeValue("value").equals("")){
                currentWeather.setHumidity(0);
            } else {
                currentWeather.setHumidity(Double.parseDouble(rootNode.getChild("humidity").getAttributeValue("value")));
            }
            currentWeather.setHumidityValue(rootNode.getChild("humidity").getAttributeValue("unit"));

            if (rootNode.getChild("pressure").getAttributeValue("value") == null || rootNode.getChild("pressure").getAttributeValue("value").equals("")){
                currentWeather.setPressure(0);
            } else {
                currentWeather.setPressure(Double.parseDouble(rootNode.getChild("pressure").getAttributeValue("value")));
            }
            currentWeather.setPressureValue(rootNode.getChild("pressure").getAttributeValue("unit"));

            Element wind = rootNode.getChild("wind");

            if (wind.getChild("speed").getAttributeValue("value") == null || wind.getChild("speed").getAttributeValue("value").equals("")){
                currentWeather.setWindSpeed(0);
            } else {
                currentWeather.setWindSpeed(Double.parseDouble(wind.getChild("speed").getAttributeValue("value")));
            }
            currentWeather.setWindName(wind.getChild("speed").getAttributeValue("name"));

            if (wind.getChild("direction").getAttributeValue("value") == null || wind.getChild("direction").getAttributeValue("value").equals("")){
                currentWeather.setDirectionValue(0);
            } else {
                currentWeather.setDirectionValue(Double.parseDouble(wind.getChild("direction").getAttributeValue("value")));
            }
            currentWeather.setDirectionName(wind.getChild("direction").getAttributeValue("name"));

            if (rootNode.getChild("clouds").getAttributeValue("value") == null || rootNode.getChild("clouds").getAttributeValue("value").equals("")){
                currentWeather.setCloudsValue(0);
            } else {
                currentWeather.setCloudsValue(Double.parseDouble(rootNode.getChild("clouds").getAttributeValue("value")));
            }

            currentWeather.setCloudsName(rootNode.getChild("clouds").getAttributeValue("name"));

            currentWeather.setIcon(rootNode.getChild("weather").getAttributeValue("icon"));

            currentWeather.setLastUpdated(rootNode.getChild("lastupdate").getAttributeValue("value"));
        }

        catch (JDOMException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}

