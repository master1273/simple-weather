package com.wiceo.simpleweather.types;

import java.util.ArrayList;

/**
 * Created by master on 07.11.2015.
 */
public class Forecast {
    private String cityName;
    private String countryName;
    private double lat;
    private double lon;
    private double calcTime;
    private ArrayList<FutureWeather> futureWeathers;

    public Forecast(){
        futureWeathers = new ArrayList<FutureWeather>();
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCalcTime(double calcTime) {
        this.calcTime = calcTime;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public double getCalcTime() {
        return calcTime;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public void setFutureWeathers(ArrayList<FutureWeather> futureWeathers) {
        this.futureWeathers = futureWeathers;
    }

    public void addFutureWeather(FutureWeather futureWeather){
        this.futureWeathers.add(futureWeather);
    }

    public ArrayList<FutureWeather> getFutureWeathers() {
        return futureWeathers;
    }

    public String getCityName() {
        return cityName;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

}
