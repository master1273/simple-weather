package com.wiceo.simpleweather;

/**
 * Created by Pavel on 05.11.2015.
 */
public class Constants {

    public static final int INFO_TAB = 0;
    public static final int MAP_TAB = 1;
    public static final int POST_DELAY_FOR_EXIT = 2000;
    public static final String TEST_DEVICE_ID = "8121183652A336875146AF9374E3DC3E";
    public static final int VOICE_RECOGNITION_REQUEST_CODE = 8855;
    public static final String FEEDBACK_EMAIL = "wiceo.apps@gmail.com";
    public static final String FEEDBACK_SUBJECT = "SimpleWeather Feedback";
}