package com.wiceo.simpleweather.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wiceo.simpleweather.ApplicationContextProvider;
import com.wiceo.simpleweather.R;
import com.wiceo.simpleweather.types.FutureWeather;
import com.wiceo.simpleweather.types.IconNameList;

import java.util.ArrayList;

/**
 * Created by maste on 07.11.2015.
 */
public class ForecastArrayAdapter extends ArrayAdapter<FutureWeather> {
    private Context context;
    private ArrayList<FutureWeather> futureWeatherArrayList;
    private int viewId;

    public ForecastArrayAdapter(Context context, ArrayList<FutureWeather> futureWeathers) {
        super(context, R.layout.forecast_list_item, futureWeathers);
        this.context = context;
        this.futureWeatherArrayList = futureWeathers;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = convertView;

        if(convertView == null) {
            rowView = inflater.inflate(R.layout.forecast_list_item, parent, false);
        }
        final VHolder holder = new VHolder();

        holder.futureWeather = futureWeatherArrayList.get(position);
        holder.time = (TextView) rowView.findViewById(R.id.forecast_time_from);
        holder.temperature = (TextView) rowView.findViewById(R.id.forecast_temperature);
        holder.pressure = (TextView) rowView.findViewById(R.id.forecast_pressure);
        holder.humidity = (TextView) rowView.findViewById(R.id.forecast_humidity);
        holder.wind = (TextView) rowView.findViewById(R.id.forecast_wind);
        holder.windDirection = (TextView) rowView.findViewById(R.id.forecast_wind_direction);
        holder.precipitation = (TextView) rowView.findViewById(R.id.forecast_precipitation);
        holder.clouds = (TextView) rowView.findViewById(R.id.forecast_clouds);
        holder.icon = (ImageView) rowView.findViewById(R.id.forecast_icon);

        rowView.setTag(holder.futureWeather);
        setupItem(holder);

        return rowView;
    }

    private void setupItem(final VHolder holder){
        if (!"".equals(holder.futureWeather.getTimeFrom())){
            holder.time.setText(holder.futureWeather.getTimeFrom().replace("T", " "));
        }

        if (holder.futureWeather.getAvrTemp() != 0){
            holder.temperature.setText(Double.toString(holder.futureWeather.getAvrTemp()) + ApplicationContextProvider.getStringRes(R.string.celsius));
        }

        if (!"".equals(holder.futureWeather.getIcon())){
            holder.icon.setImageResource(
                    context.getResources().getIdentifier(
                            IconNameList.getName(holder.futureWeather.getIcon()),
                            "drawable",
                            context.getPackageName()));
        }

        if (holder.futureWeather.getPressureValue() != 0){
            if (!"".equals(holder.futureWeather.getPressureUnit())) {
                holder.pressure.setText(Double.toString(holder.futureWeather.getPressureValue()) + " " + holder.futureWeather.getPressureUnit());
            } else {
                holder.pressure.setText(Double.toString(holder.futureWeather.getPressureValue()));
            }
        }

        if (holder.futureWeather.getHumidityValue() != 0){
            if (!"".equals(holder.futureWeather.getHumidityUnit())) {
                holder.humidity.setText(Double.toString(holder.futureWeather.getHumidityValue()) + " " + holder.futureWeather.getHumidityUnit());
            } else {
                holder.humidity.setText(Double.toString(holder.futureWeather.getHumidityValue()));
            }
        }

        if (holder.futureWeather.getWindSpeed() != 0){
            if (!"".equals(holder.futureWeather.getWindName())) {
                holder.wind.setText(holder.futureWeather.getWindName() + ", " + Double.toString(holder.futureWeather.getWindSpeed()) + " " + ApplicationContextProvider.getStringRes(R.string.windspeed));
            } else {
                holder.wind.setText(Double.toString(holder.futureWeather.getWindSpeed()) + " " + ApplicationContextProvider.getStringRes(R.string.windspeed));
            }
        }

        if (!"".equals(holder.futureWeather.getWindDirection())){
            holder.windDirection.setText(holder.futureWeather.getWindDirection());
        }

        if (holder.futureWeather.getPrecipitationValue() != 0){
            if (!"".equals(holder.futureWeather.getPrecipitationType())){
                holder.precipitation.setText(Double.toString(holder.futureWeather.getPrecipitationValue()) + ", " + holder.futureWeather.getPrecipitationType());
            } else {
                holder.precipitation.setText(Double.toString(holder.futureWeather.getPrecipitationValue()));
            }
        }

        if (holder.futureWeather.getCloudsValue() != 0){
            if (!"".equals(holder.futureWeather.getCloudsUnit())){
                holder.clouds.setText(Double.toString(holder.futureWeather.getCloudsValue()) + ", " + holder.futureWeather.getCloudsUnit());
            } else {
                holder.clouds.setText(Double.toString(holder.futureWeather.getCloudsValue()));
            }
        }
    }

    public static class VHolder {
        FutureWeather futureWeather;
        TextView time;
        TextView temperature;
        TextView humidity;
        TextView pressure;
        TextView wind;
        TextView windDirection;
        TextView precipitation;
        TextView clouds;
        ImageView icon;

    }
}
