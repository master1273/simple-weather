package com.wiceo.simpleweather.types;



/**
 * Created by maste on 06.11.2015.
 */
public class IconNameList {
    public static String getName(String code){

        switch(code){
            case "01d": return "sun";
            case "01n": return "moon";
            case "02d": return "sunwithcloud";
            case "02n": return "moonwithcloud";
            case "03d": return "scatteredday";
            case "03n": return "scatterednight";
            case "04d": return "brokenday";
            case "04n": return "brokennight";
            case "09d": return "showerday";
            case "09n": return "showernight";
            case "10d": return "rainday";
            case "10n": return "rainnight";
            case "11d": return "thunderstormday";
            case "11n": return "thunderstormnight";
            case "13d": return "snowday";
            case "13n": return "snownight";
            case "50d": return "mistday";
            case "50n": return "mistnight";
            default: return null;
        }
    }
}
