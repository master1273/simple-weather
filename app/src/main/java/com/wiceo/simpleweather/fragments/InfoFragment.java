package com.wiceo.simpleweather.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.wiceo.simpleweather.ApplicationContextProvider;
import com.wiceo.simpleweather.R;
import com.wiceo.simpleweather.Utils;
import com.wiceo.simpleweather.parsers.CurrentWeatherParser;
import com.wiceo.simpleweather.types.CurrentWeather;
import com.wiceo.simpleweather.types.IconNameList;

import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Pavel on 05.11.2015.
 */

public class InfoFragment extends Fragment {

    //TextViews start
    private TextView cityName;
    private ImageView weatherIcon;
    private TextView temperature;
    private TextView pressure;
    private TextView humidity;
    private TextView wind;
    private TextView sunRise;
    private TextView sunSet;
    private TextView lastUpdated;
    //TextVews End

    public static InfoFragment newInstance() {
        InfoFragment f = new InfoFragment();
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.info_fragment, container, false);

        GetCurrentWeather getter = new GetCurrentWeather(getLink(Utils.getDouble(ApplicationContextProvider.getPrefs(), "lat", 0),
                Utils.getDouble(ApplicationContextProvider.getPrefs(), "lon", 0)));
        getter.start();
        initComponents(view);
        return view;
    }

    private String getLink(double lat, double lon){
        return "http://api.openweathermap.org/data/2.5/weather?lat="
                + Double.toString(lat)
                + "&lon="
                + Double.toString(lon)
                + "&units=metric"
                + "&mode=xml&appid="
                + ApplicationContextProvider.getStringRes(R.string.open_weather_api_key);
    }

    private void initComponents(View view){
        cityName = (TextView) view.findViewById(R.id.info_city_name);
        weatherIcon  = (ImageView) view.findViewById(R.id.info_weather_picture);
        temperature = (TextView) view.findViewById(R.id.info_temp_avr);
        pressure = (TextView) view.findViewById(R.id.info_pressure);
        humidity = (TextView) view.findViewById(R.id.info_humidity);
        wind = (TextView) view.findViewById(R.id.info_wind);
        sunRise = (TextView) view.findViewById(R.id.info_sunrise);
        sunSet = (TextView) view.findViewById(R.id.info_sunset);
        lastUpdated = (TextView) view.findViewById(R.id.info_lastupdated);
    }

    private void fillData(CurrentWeather currentWeather){
        if (!"".equals(currentWeather.getCityName())){
            if(!"".equals(currentWeather.getCountry())){
                cityName.setText(currentWeather.getCityName() + ", " + currentWeather.getCountry());
            } else {
                cityName.setText(currentWeather.getCityName());
            }
        }

        if (!"".equals(currentWeather.getIcon())){
            weatherIcon.setImageResource(
                    getActivity().getResources().getIdentifier(
                            IconNameList.getName(currentWeather.getIcon()),
                            "drawable",
                            getActivity().getPackageName()));
        }

        if (currentWeather.getAvrTemp() != 0){
            temperature.setText(Double.toString(currentWeather.getAvrTemp()) + ApplicationContextProvider.getStringRes(R.string.celsius));
        }

        if (currentWeather.getPressure() != 0){
            if (!"".equals(currentWeather.getPressureValue())) {
                pressure.setText(Double.toString(currentWeather.getPressure()) + " " + currentWeather.getPressureValue());
            } else {
                pressure.setText(Double.toString(currentWeather.getPressure()));
            }
        }

        if (currentWeather.getHumidity() != 0){
            if (!"".equals(currentWeather.getHumidityValue())) {
                humidity.setText(Double.toString(currentWeather.getHumidity()) + " " + currentWeather.getHumidityValue());
            } else {
                humidity.setText(Double.toString(currentWeather.getHumidity()));
            }
        }

        if (currentWeather.getWindSpeed() != 0){
            if (!"".equals(currentWeather.getWindName())){
                wind.setText(currentWeather.getWindName() + " " + Double.toString(currentWeather.getWindSpeed()) + " " + ApplicationContextProvider.getStringRes(R.string.windspeed));
            } else {
                wind.setText(Double.toString(currentWeather.getWindSpeed()) + ApplicationContextProvider.getStringRes(R.string.windspeed));
            }
        }

        if (!"".equals(currentWeather.getSunRise())){
            sunRise.setText(currentWeather.getSunRise().substring(currentWeather.getSunRise().indexOf("T")+1));
        }
        if (!"".equals(currentWeather.getSunSet())){
            sunSet.setText(currentWeather.getSunSet().substring(currentWeather.getSunSet().indexOf("T")+1));
        }

        if (!"".equals(currentWeather.getLastUpdated())){
            lastUpdated.setText(currentWeather.getLastUpdated().replace("T", " "));
        }
    }

    private class GetCurrentWeather extends Thread {
        private String link;

        public GetCurrentWeather (String link){
            this.link = link;
        }

        @Override
        public void run() {
            final CurrentWeather currentWeather = new CurrentWeather();
            Boolean flag = true;
            try {
                URL url = new URL(link);
                URLConnection conn = url.openConnection();
                CurrentWeatherParser.parseCurrentWeather(conn.getInputStream(), currentWeather);

            } catch (Exception e){
                e.printStackTrace();
                flag = false;
            } finally {
                if (flag){
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            fillData(currentWeather);
                        }
                    });
                }
            }
            Thread.currentThread().interrupt();
        }
    }
}